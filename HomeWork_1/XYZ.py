'''
Написать функционал без использования каких-либо условных выражений, а применив встроенные
функции min() и max(), который принимает три аргумента - числа X, Y, Z и возвращает один из
вариантов в порядке возрастания значимости:
- X, если Y < X;
- Z, если Y > Z;
- Y, при ином раскладе.
'''
def main():
  #  print ('Последовательно введите X, Y, Z')
   # try:
      #  x=int(input())
      #  y=int(input())
      #  z=int(input())
      #  print (bool(-5))
   # except:
       # print('были введены не числа')
    s1 = 'acegikmoqsuwygndtsyfhabcdefghij'
    current = s1[0] # current = s
    result = s1[0]
    for i in range(1, len(s1)):
        if s1[i] >= current[-1]:
            current += s1[i]
            if len(current) > len(result):
                result = current
        else:
            current = s1[i]
    print(result)
if __name__=="__main__":
    main()