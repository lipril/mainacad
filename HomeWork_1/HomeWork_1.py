#Task 1
print ('Task 1: Преобразовать строку "ecnalubma" в ее зеркальное отражение.')
a = "ecnalubma"
# first method
print (a[::-1])
# second method
str_list = []
for x in (a):
    str_list.insert(0,x)
str_reverse = ''.join(str_list)
print (str_reverse)
# third method
str_list=list(a)
str_list.reverse()
str_reverse_one=''.join(str_list)
print (str_reverse_one)
# fourth method
for x in reversed (a):
    print(x, end='')
print ('')
input ('Press \'enter\' to continue...')
# следующие три строчки гордо стырены в интернете :-)
import os
clear = lambda: os.system('cls')
clear()
#Task 2
print('Task 2: подсчет гласных.')
letters = list('aeiou')
print (letters)
temp = str(input('enter any phrase: '))
count = 0
for x in (temp):
    if x.lower() in letters:
        count+=1
print ('Number of glasnych letters in prase =', count)
input ('Press \'enter\' to continue...')
clear()
#Task 3
print('Task 3: подсчет вхождений подстроки \"wow\" в строку\n\"wowhatamanwowowpalehchealaportwowfjgwowwow\".')
s = ("wowhatamanwowowpalehchealaportwowfjgwowwow")
count = 0
for x in range(0, len(s)-2):
    if s[x: x+3] == 'wow':
        count += 1
print ('кол-во вхождений подстроки wow =',count)
input ('Press \'enter\' to continue...')
clear()
#Task 4
print('Task 4: упорядоченная подстрока.\n решения пока нет')
input ('Press \'enter\' to continue...')
clear()
#Task 5
print('Task 5: определение типа.')
def func (obj):
    print(obj,type(obj))
func(666)
func("666")
func(func)
func([])
func({})
input ('Press \'enter\' to continue...')
clear()
#Task 6
print('Task 6: А & B.')
#Написать фрагмент python кода, который будет анализировать две переменные (А и В), которые
#могут быть типа "str" или "int".
#В зависимости от значения переменных код должен выводить на печать ОДНО из следующих
#сообщений:
#- "получена строка" - если хотя бы одна из переменных является строкой;
#- "больше" - если А больше В;
#- "равны" - если значения переменных равны;
#- "меньше" - если А меньше В.
try:
    A = int(input("Enter \"A\": "))
    B = int(input("Enter \"B\": "))
    if A > B:
        print ('больше')
    elif A==B:
        print ('равны')
    else:
        print ('меньше')
except:
    print ("получена строка")
input ('Press \'enter\' to continue...')
clear()
# Task 7
print('Task 7: уникальный набор.')
#Реализовать функцию, которая принимает список елементов и убирает из него все дубликаты
#(формирует список уникальных элементов).
#Сделать вариант с сохранением порядка следования элементов и вариант, в кот. сортировка
#элементов не принципиальна.
#Пример:
#а) assert unique_ordered(["a", 5, 2, 5, (1, "a"), "a"]) == ["a", 5, 2, (1, "a")]
#б) assert unique_disordered(["a", 5, 2, 5, (1, "a"), "a"]) == [2, "a", 5, (1, "a")]
array = ["a", 5, 2, 5, (1, "a"), "a"]
print (array)
sort_array = list()
for x in array:
    if not x in sort_array:
        sort_array.append(x)
print ('unique array', sort_array)
s=set(array)
print ('set',s)
input ('Press \'enter\' to continue...')
clear()
# Task 8
print('Task 8: каждый третий.')
#УСЛОВИЕ:
#Реализовать функционал который принимает кортеж и возвращает прореженный кортеж, оставляя
#только каждый третий элемент.
#Реализовать задачу двумя разными вариантами.
#Пример:
#t = (1,2,3,4,5,6,7,8,9,0,'a','b','c')
#> (3,6,9,'b')
t = (1,2,3,4,5,6,7,8,9,0,'a','b','c')
print (t)
new_t=t[2::3]
print (new_t)
sorted = list()
for x in range (len(t)+1,1,-1):
    if x%3==0:
        sorted.insert(0,t[x-1])
# print (sorted)
newest_t=tuple(sorted)
print (newest_t)
input ('Press \'enter\' to continue...')
clear()
# Task 9
print('Task 9: XYZ.')
#Написать функционал без использования каких-либо условных выражений, а применив встроенные
#функции min() и max(), который принимает три аргумента - числа X, Y, Z и возвращает один из
#вариантов в порядке возрастания значимости:
#- X, если Y < X;
#- Z, если Y > Z;
#- Y, при ином раскладе.
print ('Последовательно введите X, Y, Z')
try:
    x=int(input())
    y=int(input())
    z=int(input())
    print ('решения пока нет')
except:
    print('были введены не числа')
