#Посчитать количество гласных в каждом слове текста.
#Вывести максимальное количество гласных в одном слове.
#Текст:
#Proin eget tortor risus. Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus. Curabitur non
#nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada.
#Гласные: A, E, I, O, U, Y
def glas (text):    
    print ('Task 1\n',text,'\n')
    letters = ['a','e','i','o','u','y']    
    temp = text.lower()
    temp = temp.split()    
    count = []
    for i in temp:
        count_tmp=0
        for j in i:
            if j in letters:
                count_tmp+=1
        count.append(count_tmp)
    print('максимальное кол-во гласных букв',max(count))