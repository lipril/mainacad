import os
clear = lambda: os.system('cls')
#Task 1
#Создать функцию, которая принимает набор (список, кортеж) чисел и возвращает набор
#ТОГО ЖЕ типа со значениями квадратов этих чисел.
#Пример:
#[1,2,3] >> [1,4,9]
#(1,2,3) >> (1,4,9)
def sqr (obj):
    rez = list()
    for i in obj:
        rez.append(i**2)
    if type(obj) is tuple:        
        return tuple(rez)
    else: 
        return rez
#a = list(1,2,3,4)
#b = tuple(3,6,9)
a = [1,2,4,8]
b = (3,6,9)
print(sqr(a), type(a))
print(sqr(b), type(b))
input ('Press \'enter\' to continue...')
clear()
#Task 2
#Создать функцию, которая принимает строку и определяет симметрична ли строка.
#(Возвращает True или False).
#Пример:
#"abba" >> True
#"arbat" >> False
def symmetry (obj):
    return obj == obj[::-1]
s_one = 'abba'
s_two = 'arbat'
print(symmetry(s_one))
print(symmetry(s_two))
input ('Press \'enter\' to continue...')
clear()
#Task 3
#Фрагмент кода, который принимает список любых чисел и возвращает словарь вида:
#{число: boolean}, где: boolean - True или False в зависимости делится ли число на 5
#без остатка.
#Пример:
#[5,25,39] >> {5:True, 25:True, 39:False}
arr = [5,25,39]
print(dict((i,(i%5==0)) for i in arr))
input ('Press \'enter\' to continue...')
clear()
#Task 4
#Фрагмент кода, который принимает список любых чисел и фильтрует его по четным (удаляет
#нечетные),
#если количество элементов в списке является четным и наоборот (удаляет четные, если элементов
#изначально нечетное количество).
#Пример:
#[3,7,12] >> [3,7]
#[3,7,12,7] >> [12]
l1 = [3,7,12]
l2 = [3,7,12,7]
def func (obj):
    rez = list()
    for i in obj:
        if (len(obj)%2==0) and i%2==0:
            rez.append(i)
        elif (len(obj)%2!=0) and i%2!=0:
            rez.append(i)
    print(rez)
func(l1)
func(l2)
input ('Press \'enter\' to continue...')
clear()
#Task 5
#Фрагмент кода, который принимает список любых чисел и модифицирует его следующим образом:
#- в начале списка идут нечетные числа в порядке возрастания,
#- далее идут четные числа в порядке убывания.
#Пример:
#[1,4,8,6,3,7,1] >> [1,1,3,7,8,6,4]
l3 = [1,4,8,6,3,7,1]
rez = list()
for i in l3:
    if i%2 == 0:
        rez.append(i)
        l3.remove(i)
l3.sort()
rez.sort()
rez.reverse()
l3+=rez
print(l3)
input ('Press \'enter\' to continue...')
clear()
#Task 6
#УСЛОВИЕ:
#Фрагмент кода, который принимает словарь со значениями элементов разных типов,
#а возвращает словарь вида {имя_типа : количество_элементов_этого_типа}.
#Пример:
#{'a': 1, 3: [1,5], 'e': 'abc', '6': []} >> {'str': 1, 'list': 2, 'int': 1}
d = {'a': 1, 3: [1,5], 'e': 'abc', '6': []}
print('no solution')
input ('Press \'enter\' to continue...')
clear()
#Task 8
#Фрагмент кода, который принимает кортеж любых чисел и модифицирует его
#в кортеж кортежей по два элемента (парами).
#Пример:
#(1,4,8,6,3,7,1) >> ((1,4),(8,6),(3,7),(1,))
t = (1,4,8,6,3,7,1)
rez = list()
for i in range (0, len(t),2):
    #print(t[i])
    temp = t[i:i+2]
    rez.append(temp)
print(tuple(rez))
input ('Press \'enter\' to continue...')
clear()
#Task 9
#Фрагмент кода, который принимает список списков, и делает список "плоским" -
#разворачивая элементы внутренних списков во вмещающий список.
#Пример:
#[[1],[4,8],[6,3,7],[1,3]] >> [1,4,8,6,3,7,1,3]
l = [[1],[4,8],[6,3,7],[1,3]]
rez = list()
for i in l:
    for j in i:
        rez.append(j)
print(rez)
